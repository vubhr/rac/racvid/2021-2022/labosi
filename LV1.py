import numpy as np
import cv2 as cv
import sys


def task_01():
    image = np.array([[1., 7., 4., 8., 11.],
                      [3., 1., 2., 4., 5.],
                      [11., 14., 22., 3., 5.],
                      [1., 7., 4., 8., 11.],
                      [3., 1., 2., 4., 5.]])

    print("Data: ", image)
    print("Dimensions: ", image.ndim)
    print("Shape: ", image.shape)
    print("Size: ", image.size)
    print("Dtype: ", image.dtype)
    print("Item size: ", image.itemsize)
    print("Image size: ", image.itemsize * image.size, " bytes")


def task_02():
    img = cv.imread("Images/Albert.jpg")
    if img is None:
        sys.exit("Could not read the image.")

    cv.imshow("Display window", img)
    cv.waitKey(0)
    cv.destroyAllWindows()

def task_03():
    img = cv.imread("Images/Albert.jpg")
    if img is None:
        sys.exit("Could not read the image.")
    cv.imshow("Albert", img)
    print("width: {} pixels".format(img.shape[1]))
    print("height: {} pixels".format(img.shape[0]))
    print("channels: {}".format(img.shape[2]))
    print((img.shape[0]*img.shape[1] * img.shape[2]))
    cv.waitKey(0)

def task_04():
    img = cv.imread("Images/Albert.jpg")
    if img is None:
        sys.exit("Could not read the image.")
    cv.imshow("Albert", img)
    cv.imwrite("Test.png", img)
    cv.waitKey(0)

def task_05():
    img = cv.imread("Images/Formula1.jpg", cv.IMREAD_UNCHANGED)
    if img is None:
        sys.exit("Could not read the image.")
    print(img[100, 100])
    img[100, 100] = [0, 0, 0]
    img[100, 102] = [0, 0, 0]
    img[100, 103] = [0, 0, 0]
    cv.namedWindow("Formula", cv.WINDOW_FREERATIO)
    cv.imshow("Formula", img)

    cv.waitKey(0)


